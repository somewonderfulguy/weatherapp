#!/usr/bin/env bash

sudo pip3 install virtualenv
mkdir env
virtualenv env
source env/bin/activate

pip3 install -r requirements.txt

python3 proxy.py