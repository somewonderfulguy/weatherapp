from flask import Flask, jsonify, request
import requests

app = Flask(__name__)
remote_addr = 'http://api.openweathermap.org'

@app.route('/<path:url>', methods=["OPTIONS"])
def preflight(url):
	return jsonify({"status": True})

@app.route('/<path:url>', methods=["GET"])
def proxy(url):
	r = requests.get(f'{remote_addr}/{url}?{request.query_string.decode("utf-8")}')
	return jsonify(r.json())

if __name__ == '__main__':
	app.run(host='0.0.0.0', port=3001, debug=False)