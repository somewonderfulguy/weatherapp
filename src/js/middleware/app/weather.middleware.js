import {GET_WEATHER, setWeather, WEATHER} from "../../actions/weather.actions";
import {SET_CURRENT_CITY} from "../../actions/cities.actions";
import {API_ERROR, API_SUCCESS, apiRequest} from "../../actions/api.actions";
import {SET_UNITS} from "../../actions/units.actions";
import {setLoader} from "../../actions/loaders.actions";
import {addError, removeError} from "../../actions/errors.actions";

export default ({dispatch, getState}) => next => action => {
    next(action);

    const {cities: {current: city}, units: {current: units}} = getState();

    switch (action.type) {
        case SET_CURRENT_CITY:
        case SET_UNITS:
        case GET_WEATHER:
            dispatch(setLoader({weather: true}));
            dispatch(apiRequest(
                WEATHER,
                `/data/2.5/weather?q=${city}&appid=${process.env.REACT_APP_API_KEY}&units=${units.value}`,
                'GET',
                false,
                city
            ));
            break;
        case `${WEATHER} ${API_SUCCESS}`:
            dispatch(setWeather({[action.extra]: action.payload}));
            dispatch(setLoader({weather: false}));
            dispatch(removeError('weather'));
            break;
        case `${WEATHER} ${API_ERROR}`:
            dispatch(setLoader({weather: false}));
            dispatch(addError('weather'));
            break;
        default:
            return;
    }
}