import {apiSuccess, apiError} from '../../actions/api.actions';


export default ({dispatch}) => next => action => {
    next(action);

    if (action.type.includes('API_REQUEST')) {
        const {type, url, method, extra} = action.meta;

        const headers = {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
        };

        fetch(
            `${process.env.REACT_APP_SERVER_URL}${url}${method === 'GET' && action.payload 
                ? `?${Object.keys(action.payload).map((x) => `${x}=${action.payload[x]}`).join('&')}` 
                : ''}`,
            {
                method: method,
                credentials: 'omit',
                headers: headers,
                body: method !== 'GET' ? JSON.stringify(action.payload) : null,
            })
            .then(resp => {
                if (!resp.ok)
                    throw new Error();
                return resp.json()
            })
            .then(data => dispatch(apiSuccess(type, data, extra)))
            .catch(err => dispatch(apiError(type, err)));
    }

    return true
};