export const CITIES = '[CITIES]';

export const SET_CURRENT_CITY = `${CITIES} Set current`;

export const setCurrentCity = (city) => ({
    type: SET_CURRENT_CITY,
    payload: city
});