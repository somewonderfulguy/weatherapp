export const UNITS = '[UNITS]';

export const SET_UNITS = `${UNITS} Set`;

export const setUnits = (unit) => ({
    type: SET_UNITS,
    payload: unit
});