export const API_REQUEST = 'API_REQUEST';
export const API_SUCCESS = 'API_SUCCESS';
export const API_ERROR = 'API_ERROR';

export const apiRequest = (
    type,
    url = '',
    method = 'GET',
    payload = false,
    extra=false
) => ({
    type: `${type} ${API_REQUEST}`,
    payload: payload,
    meta: {
        type,
        url,
        method,
        extra
    }
});

export const apiSuccess = (type, payload, extra=false) => ({
    type: `${type} ${API_SUCCESS}`,
    payload: payload,
    extra: extra
});

export const apiError = (type, payload) => ({
    type: `${type} ${API_ERROR}`,
    payload: payload
});