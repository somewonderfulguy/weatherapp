export const ERROR = '[ERROR]';

export const ADD_ERROR = `${ERROR} Add`;
export const REMOVE_ERROR = `${ERROR} Remove`;

export const addError = (error) => ({
    type: ADD_ERROR,
    payload: error
});

export const removeError = (error) => ({
    type: REMOVE_ERROR,
    payload: error
});