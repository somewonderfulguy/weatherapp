export const LOADERS = '[LOADERS]';

export const SET_LOADER = `${LOADERS} Set`;

export const setLoader = (data) => ({
    type: SET_LOADER,
    payload: data
});