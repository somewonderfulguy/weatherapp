export const WEATHER = '[WEATHER]';

export const SET_WEATHER = `${WEATHER} Set`;
export const GET_WEATHER = `${WEATHER} Get`;

export const getWeather = () => ({
    type: GET_WEATHER
});

export const setWeather = (data) => ({
    type: SET_WEATHER,
    payload: data
});