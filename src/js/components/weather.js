import React from 'react';
import {connect} from 'react-redux';
import moment from 'moment';
import {get} from 'lodash';

const unitsDescription = {
    '': {temp: 'K', wind: "meter/sec"},
    'metric': {temp: 'C', wind: "meter/sec"},
    'imperial': {temp: 'F', wind: "miles/hour"}
};

const Weather = ({city, units, weather, loader, error}) => {
    const data = get(weather, [city], {
        weather: [{description: 'Loading...'}],
        wind: {speed: 0},
        main: {humidity: 0, temp: 0, pressure: 0},
        dt: moment
    });

    return (
        <div className={"weather"}>
            <div className={`weather__loading ${!loader && 'hidden'}`}/>
            <div className={"weather__top"}>
                <div className={"weather__text weather__text_large"}>{city}</div>
                <div className={'weather__text'}>
                    Today, {moment(data.dt).format("hh:mm")}
                    {error && <span className={'weather__text weather__text_error'}>
                        Unable to update data (connection failed), try again later
                    </span>}
                </div>
                <div className={"weather__text"}>{data.weather[0].description}</div>
            </div>
            <div className={"weather__body"}>
                <div className={"weather__main"}>
                    <div className={"weather__visual"}>
                        <img
                            src={`https://openweathermap.org/img/w/${data.weather[0].icon}.png`}
                            alt={""}
                            className={"weather__icon"}
                        />
                    </div>
                    <div className={"weather__temperature"}>{data.main.temp} {unitsDescription[units].temp}</div>
                </div>
                <div className={"weather__info"}>
                    <div className={"weather__text"}>Humidity: {data.main.humidity} %</div>
                    <div className={"weather__text"}>Wind speed: {data.wind.speed} {unitsDescription[units].wind}</div>
                    <div className={"weather__text"}>Pressure: {data.main.pressure} hPa</div>
                </div>
            </div>
        </div>
    );
};

const mapStateToProps = (state) => ({
    weather: state.weather,
    city: state.cities.current,
    units: state.units.current.value,
    loader: state.loaders.weather,
    error: state.errors.includes('weather')
});

export default connect(
    mapStateToProps
)(Weather);
