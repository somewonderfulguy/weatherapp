import React from 'react';
import Select from 'react-select';
import {connect} from 'react-redux';

import {setCurrentCity} from "../actions/cities.actions";
import {setUnits} from "../actions/units.actions";

const Nav = ({cities: {list: cities, current: city}, units: {list: units, current: unit}, setCity, setUnit}) => (
    <div className={"nav"}>
        <div className={"nav__name"}>Weather App</div>
        <div className={"nav__right"}>
            <Select
                value={unit}
                options={units}
                isSearchable
                onChange={(value) => setUnit(value)}
                className={"nav__select"}
            />
            <Select
                value={{value: city, label: city}}
                options={cities.map(x => ({value: x, label: x}))}
                isSearchable
                onChange={(value) => setCity(value.value)}
                className={"nav__select"}
            />
        </div>
    </div>
);

const mapStateToProps = (state) => ({
    cities: state.cities,
    units: state.units
});

const mapDispatchToProps = (dispatch) => ({
    setCity: (city) => dispatch(setCurrentCity(city)),
    setUnit: (unit) => dispatch(setUnits(unit))
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Nav);
