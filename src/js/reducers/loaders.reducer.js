import {SET_LOADER} from "../actions/loaders.actions";

export default (state={}, action) => {
    switch (action.type) {
        case SET_LOADER:
            return {
                ...state,
                ...action.payload
            };
        default:
            return state;
    }
};