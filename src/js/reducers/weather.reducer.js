import {SET_WEATHER} from "../actions/weather.actions";

const initialState = {};

export default (state=initialState, action) => {
    switch (action.type) {
        case SET_WEATHER:
            return {
                ...state,
                ...action.payload
            };
        default:
            return state;
    }
};