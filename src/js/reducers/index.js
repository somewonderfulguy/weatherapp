import {combineReducers} from "redux";

import citiesReducer from './cities.reducer';
import unitsReducer from './units.reducer';
import weatherReducer from './weather.reducer';
import loadersReducer from './loaders.reducer';
import errorsReducer from './errors.reducer';

export default combineReducers({
    cities: citiesReducer,
    units: unitsReducer,
    weather: weatherReducer,
    loaders: loadersReducer,
    errors: errorsReducer
});