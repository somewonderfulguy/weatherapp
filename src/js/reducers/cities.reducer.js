import {SET_CURRENT_CITY} from "../actions/cities.actions";

const initialState = {
    list: [
        'London',
        'Stockholm',
        'Kyiv'
    ],
    current: "London"
};

export default (state = initialState, action) => {
    switch (action.type) {
        case SET_CURRENT_CITY:
            return {
                ...state,
                current: action.payload
            };
        default:
            return state;
    }
}