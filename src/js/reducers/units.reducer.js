import {SET_UNITS} from "../actions/units.actions";

const initialState = {
    list: [
        {value: "", label: "Kelvins"},
        {value: "metric", label: "Celsius"},
        {value: "imperial", label: "Fahrenheits"}
    ],
    current: {value: "", label: "Kelvins"}
};

export default (state=initialState, action) => {
    switch (action.type) {
        case SET_UNITS:
            return {
                ...state,
                current: action.payload
            };
        default:
            return state;
    }
};