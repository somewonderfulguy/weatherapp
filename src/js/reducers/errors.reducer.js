import {ADD_ERROR, REMOVE_ERROR} from "../actions/errors.actions";

export default (state=[], action) => {
    switch (action.type) {
        case ADD_ERROR:
            return [...state, action.payload];
        case REMOVE_ERROR:
            return state.filter(x => x !== action.payload);
        default:
            return state;
    }
};