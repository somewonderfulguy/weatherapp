import React, {Component} from 'react';
import {connect} from 'react-redux';

import Nav from "./components/nav";
import Content from "./components/content";
import {getWeather} from "./actions/weather.actions";
import Weather from "./components/weather";

class App extends Component {
    componentDidMount() {
        this.props.getWeather();
    }

    render() {
        return (
            <div>
                <Nav/>
                <Content>
                    <Weather/>
                </Content>
            </div>
        );
    }
}

const mapDispatchToProps = (dispatch) => ({
    getWeather: () => dispatch(getWeather())
});

export default connect(
    null,
    mapDispatchToProps
)(App);
