import {applyMiddleware, createStore, compose} from 'redux';
import appMiddleware from './middleware/app';
import coreMiddleware from './middleware/core';
import rootReducer from './reducers/index';

const middleware = [
    ...appMiddleware,
    ...coreMiddleware
];

export default (initialState={}) => createStore(
    rootReducer,
    initialState,
    compose(applyMiddleware(...middleware))
);