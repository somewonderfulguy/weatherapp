# Weather App

### Requirements:

* Python 3
* pip3
* NodeJS
* npm

## Steps to run:

* ### Clone project
    `$ git@gitlab.com:ol_kovalenko/weatherapp.git`

* ### Start proxy server

    ```bash
    ~$ cd ./weatherapp/proxy
    ~/weatherapp/proxy$ ./init.sh
    ```

    Don't close this terminal window

* ### Start front-end side (second terminal)

    ```bash
    ~/weatherapp$ npm i
    ~/weatherapp$ npm start
    ```

    Open `localhost:3000` in your browser